
public class Sheep {

   enum Animal {sheep, goat}
   
   public static void main (String[] param) {
      // for debugging
   }
   
   public static void reorder (Animal[] animals) {
      if (animals.length < 2) return;
      int i = 0;
      int j =  animals.length - 1;
      while (j >= 0 && animals[j] == Animal.sheep) {
         j--;
      }
      while (i <= j) {
         if (animals[i] == Animal.sheep) {
            animals[i] = animals[j];
            animals[j] = Animal.sheep;
            j--;
            while (j >= 0 && animals[j] == Animal.sheep) {
               j--;
            }
         }
         i++;
      }
   }
}

